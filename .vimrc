"set nocompatible
filetype indent plugin on
set smartindent
syntax on
"set hidden
"set wildmenu
"set showcmd
"set hlsearch
"set ignorecase
"set smartcase
"set backspace=indent,eol,startpumvisible() ? coc#_select_confirm() : "\\
"\
"set autoindent
"set ruler
"set laststatus=2
"set confirm
"set visualbell
"set t_vb=
"set mouse=a
"set cmdheight=2
"set number
set pastetoggle=<F2>
"set shiftwidth=4
"set softtabstop=4
set expandtab
"I'm illiterate
setlocal spell
"map Y y$
"nnoremap <C-L> :nohl<CR><C-L>
" Note: Skip initialization for vim-tiny or vim-small.
if !1 | finish | endif

"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=/Users/matt/.local/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('/Users/matt/.local/dein')
  call dein#begin('/Users/matt/.local/dein')

  " Let dein manage dein
  " Required:
  call dein#add('/Users/matt/.local/dein/repos/github.com/Shougo/dein.vim')

  call dein#add('Raimondi/delimitMate')
  call dein#add('christoomey/vim-tmux-navigator')
  call dein#add('pangloss/vim-javascript')
  call dein#add('elzr/vim-json')
  call dein#add('mxw/vim-jsx')
  call dein#add('fatih/vim-go')
  call dein#add('godlygeek/tabular')
  call dein#add('tpope/vim-endwise')
  call dein#add('tpope/vim-sensible')
  call dein#add('tpope/vim-surround')
  call dein#add('vim-airline/vim-airline')
  call dein#add('vim-airline/vim-airline-themes')
  "call dein#add('vim-ruby/vim-ruby')
  "call dein#add('vim-scripts/CmdlineComplete')
  call dein#add('Chiel92/vim-autoformat')
  call dein#add('scrooloose/nerdtree')
  call dein#add('hashivim/vim-terraform')
  call dein#add('dense-analysis/ale')
  call dein#add('prettier/vim-prettier', {'build': 'yarn install'})
  "call dein#add('Glench/Vim-Jinja2-Syntax')
  call dein#add('neoclide/coc.nvim', { 'merged': 0, 'rev': 'release' })

  call dein#end()
  call dein#save_state()
endif

let g:dein#types#git#default_protocol='ssh'

set background=dark
colorscheme solarized
let g:solarized_termcolors = 256
set termguicolors

set number
set mouse=a

" Disable arrow keys
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

set wildmode=longest,list,full
set wildmenu

" jk is escape
inoremap jk <esc>
inoremap <esc> <nop>

" vim-airline options
let g:airline_powerline_fonts = 1
let g:airline_solarized_bg='dark'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#ale#enabled = 1

set autoindent
"set tabstop=8
"set softtabstop=8
"set shiftwidth=4
set expandtab
" set JS to 2 spaces
autocmd FileType javascript,jsx,json,html,js setlocal ts=2 sts=2 sw=2 expandtab autoindent

" ALE config
let g:ale_open_list = 1
let g:ale_set_quickfix = 1
let g:ale_set_loclist = 0
let g:ale_fix_on_save = 1
let g:ale_virtualtext_cursor = 0
let g:ale_sign_error = '❌'
let g:ale_sign_warning = '⚠️ '
let g:ale_javascript_prettier_use_global = 1

let g:ale_linters_explicit = 1
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 1
let g:ale_linters = {}
let g:ale_linters.javascript = ['eslint', 'trim_whitespace', 'remove_trailing_lines']
let g:ale_linters.html = ['prettier', 'eslint']
let g:ale_linters.sql = ['sqlint']

let g:ale_fixers_explicit = 1
let g:ale_fixers = {}
let g:ale_fixers.html = ['prettier', 'trim_whitespace', 'remove_trailing_lines']
let g:ale_fixers.javascript = ['eslint', 'trim_whitespace', 'remove_trailing_lines']
let g:ale_fixers.sql = ['pgformatter']
let g:ale_fixers['*'] =  ['trim_whitespace', 'remove_trailing_lines']

" Easier Split Navigation
" We can use different key mappings for easy navigation between splits to save
" a keystroke. So instead of ctrl-w then j, it’s just ctrl-j:
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Format json
au FileType json setlocal equalprg=python\ -m\ json.tool

" terraform plugin config
let g:terraform_align=1
let g:terraform_fmt_on_save=1
let g:terraform_fold_sections=0
let g:terraform_remap_spacebar=1

" Detect evil unicodes
" highlight nonascii guibg=Red ctermbg=1 term=standout
"au BufReadPost * syntax match nonascii "[^\u0000-\u007F]"

" Auto format web files on save
"au BufWrite *.js,*.html,*.css :call JsBeautify()

" Dont convert tab to space for makefiles
autocmd FileType make setlocal noexpandtab

" Toggle NerdTree pane
map <C-n> :NERDTreeToggle<CR>

" easier navigation between panes
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" CoC settings
" Set internal encoding of vim, not needed on neovim, since coc.nvim using some
" unicode characters in the file autoload/float.vim
set encoding=utf-8

" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("nvim-0.5.0") || has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"pumvisible() ? coc#_select_confirm(): "\
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
