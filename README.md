# dotfiles
## Setting up a new machine

1. Clone this repo
1. Copy dotfiles to home dir
```
for $file in $(ls -a .);
do;
        ln -s $file ~/$file
done;
```
1. Install Homebrew
1. Install gpg agent and pinenetry-curses
```
brew install gpg-agent;
brew install pinentry-curses;
```
1. Import public gpg key
```
gpg --import $MY_KEY_ID
```
1. Trust my gpg key
```
gpg --edit-key $MY_KEY_ID;
gpg> trust
gpg> 5
gpg> quit
```
1. Plug in yubikey and add stub keyring
```
gpg --card-status
```

# Git signing
```
# set signing key to use globally
git config --global user.signingkey F8A3E0690C7027B9162C297F462FD982BD7CAC48

# set signing on a per project basis
git config commit.gpgsign true

```
